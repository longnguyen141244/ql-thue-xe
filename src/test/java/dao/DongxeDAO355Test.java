package dao;

import model.Dongxe355;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DongxeDAO355Test {
    DongxeDAO355 dxDAO = new DongxeDAO355();

    @Test
    public void taoHopdongKygui_testChuan1() {
        Integer idHang = 1;

        try{
            List<Dongxe355> listDX = dxDAO.getDSDongxe(idHang);
            Assert.assertEquals(listDX.size(), 2);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }


    @Test
    public void taoHopdongKygui_ngoaiLe1() {
        Integer idHang = 10;

        try{
            List<Dongxe355> listDX = dxDAO.getDSDongxe(idHang);
            Assert.assertEquals(listDX.size(), 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }
}
