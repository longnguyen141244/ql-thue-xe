package dao;

import model.Hangxe355;
import model.Oto355;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class OtoDAO355Test {
    OtoDAO355 otoDAO = new OtoDAO355();

    @Test
    public void taoHopdongKygui_testChuan1() {

        try{
            List<Integer> listSoGhe = otoDAO.getDSSoghe();
            Assert.assertEquals(listSoGhe.size(), 2);
            Assert.assertEquals(listSoGhe.get(0), new Integer("5"));
            Assert.assertEquals(listSoGhe.get(1), new Integer("7"));
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan2() {
        int idDoitac = 1;

        try{
            List<Oto355> listXe = otoDAO.getDSXe(idDoitac);
            Assert.assertEquals(listXe.size(), 4);

        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_ngoaiLe1() {
        int idDoitac = 10;

        try{
            List<Oto355> listXe = otoDAO.getDSXe(idDoitac);
            Assert.assertEquals(listXe.size(), 0);

        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

}
