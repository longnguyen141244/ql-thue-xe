package dao;

import model.HopdongKygui355;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.sql.Timestamp;

public class HopdongKyguiDAO355Test {
    HopdongKyguiDAO355 hdDAO = new HopdongKyguiDAO355();

    @Test
    public void taoHopdongKygui_testChuan1() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-07"),
                Date.valueOf("2022-12-08"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testChuan2() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-25"),
                Date.valueOf("2022-12-27"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertTrue(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testChuan3() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-25"),
                Date.valueOf("2022-12-27"),
                10,
                2
        );

        HopdongKygui355 hd3 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-18"),
                Date.valueOf("2022-12-20"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertTrue(ok2);

            boolean ok3 = hdDAO.taoHopdongKygui(hd3);
            Assert.assertTrue(ok3);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe1() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe2() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-19"),
                Date.valueOf("2022-12-26"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe3() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-19"),
                Date.valueOf("2022-12-21"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe4() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-19"),
                Date.valueOf("2022-12-24"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe5() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-24"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Test
    public void taoHopdongKygui_testNgoaiLe6() {
        HopdongKygui355 hd = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-20"),
                Date.valueOf("2022-12-25"),
                10,
                2
        );

        HopdongKygui355 hd2 = new HopdongKygui355(
                0,
                new Timestamp(System.currentTimeMillis()),
                "Xe không có lỗi",
                1500000,
                Date.valueOf("2022-12-24"),
                Date.valueOf("2022-12-26"),
                10,
                2
        );

        try{
            DAO.con.setAutoCommit(false);
            boolean ok = hdDAO.taoHopdongKygui(hd);
            Assert.assertTrue(ok);

            boolean ok2 = hdDAO.taoHopdongKygui(hd2);
            Assert.assertFalse(ok2);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                DAO.con.rollback();
                DAO.con.setAutoCommit(true);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
