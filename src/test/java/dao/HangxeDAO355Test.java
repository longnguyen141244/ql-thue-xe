package dao;

import model.Dongxe355;
import model.Hangxe355;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class HangxeDAO355Test {
    HangxeDAO355 hxDAO = new HangxeDAO355();

    @Test
    public void taoHopdongKygui_testChuan1() {

        try{
            List<Hangxe355> listHX = hxDAO.getDSHangxe();
            Assert.assertEquals(listHX.size(), 6);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

}
