package dao;

import model.Doitac355;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DoitacDAO355Test {
    DoitacDAO355 dtDAO = new DoitacDAO355();

    @Test
    public void taoHopdongKygui_testChuan1() {
        Integer soGhe = 5;
        Integer idHang = 1;
        Integer idDongxe = 1;
        String ten = "accent";

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 1);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan2() {
        Integer soGhe = 5;
        Integer idHang = null;
        Integer idDongxe = null;
        String ten = null;

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 5);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan3() {
        Integer soGhe = null;
        Integer idHang = 1;
        Integer idDongxe = null;
        String ten = null;

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 3);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan4() {
        Integer soGhe = null;
        Integer idHang = null;
        Integer idDongxe = 1;
        String ten = null;

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 1);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan5() {
        Integer soGhe = null;
        Integer idHang = null;
        Integer idDongxe = null;
        String ten = "accent";

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 1);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_testChuan6() {
        Integer soGhe = null;
        Integer idHang = null;
        Integer idDongxe = null;
        String ten = null;

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 6);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void taoHopdongKygui_ngoaiLe1() {
        Integer soGhe = 7;
        Integer idHang = 1;
        Integer idDongxe = 1;
        String ten = null;

        try{
            List<Doitac355> listDT = dtDAO.timDoitac(soGhe, idHang, idDongxe, ten);
            Assert.assertEquals(listDT.size(), 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }
}
