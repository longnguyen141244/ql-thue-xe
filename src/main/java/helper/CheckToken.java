package helper;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import jakarta.servlet.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Thanhvien355;

import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(filterName = "CheckToken", urlPatterns = {"/*"})
public class CheckToken implements Filter {
    private Gson gson;

    public void init(FilterConfig config) throws ServletException {
        gson = new Gson();
    }

    private static final String[] allowedOrigins = {
            "http://localhost:3000", "http://localhost:3001", "http://localhost:5501",
            "http://127.0.0.1:3000", "http://127.0.0.1:3001", "http://127.0.0.1:5501"
    };

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String requestOrigin = httpRequest.getHeader("Origin");
        if(isAllowedOrigin(requestOrigin)) {
            // Authorize the origin, all headers, and all methods
            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", requestOrigin);
            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers", "*");
            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods",
                    "GET, OPTIONS, HEAD, PUT, POST, DELETE");

            // CORS handshake (pre-flight request)
            if (httpRequest.getMethod().equals("OPTIONS")) {
                httpResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
                return;
            }
        }

        if (!httpRequest.getRequestURI().contains("/admin")) {
            chain.doFilter(httpRequest, response);
            return;
        }

        String token = httpRequest.getHeader("Authorization");
        try {
            Algorithm algorithm = Algorithm.HMAC256("jwt_key_1412");
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            DecodedJWT decodedJWT = verifier.verify(token);

            String userJson = decodedJWT.getIssuer();
            AuthenticatedRequest authRequest = new AuthenticatedRequest(httpRequest, gson.fromJson(userJson, Thanhvien355.class));
            chain.doFilter(authRequest, response);
        } catch (JWTVerificationException exception){
            httpResponse.setContentType("application/json");
            httpResponse.setStatus(401);
            PrintWriter out = httpResponse.getWriter();
            out.println(gson.toJson(new ErrorResponse("Unauthorized error!")));
        }
    }

    private boolean isAllowedOrigin(String origin){
        for (String allowedOrigin : allowedOrigins) {
            if(origin != null && origin.equals(allowedOrigin)) return true;
        }
        return false;
    }
}
