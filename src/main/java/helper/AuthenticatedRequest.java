package helper;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import model.Thanhvien355;

public class AuthenticatedRequest extends HttpServletRequestWrapper {
    private Thanhvien355 u;

    public AuthenticatedRequest(HttpServletRequest req, Thanhvien355 u) {
        super(req);
        this.u = u;
    }

    public Thanhvien355 getUser() {
        return u;
    }
}
