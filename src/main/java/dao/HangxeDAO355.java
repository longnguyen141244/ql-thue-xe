package dao;

import model.Hangxe355;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HangxeDAO355 extends DAO {
    public HangxeDAO355() {
        super();
    }

    public List<Hangxe355> getDSHangxe() throws SQLException {
        String sql = "Select * from hangxe355";
        CallableStatement cs = con.prepareCall(sql);
        ResultSet rs = cs.executeQuery();
        List<Hangxe355> dsHangXe = new ArrayList();

        while (rs.next()) {
            Hangxe355 hangXe = new Hangxe355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getString("moTa")
            );

            dsHangXe.add(hangXe);
        }
        return dsHangXe;
    }
}
