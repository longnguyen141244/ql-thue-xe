package dao;

import model.Thanhvien355;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ThanhvienDAO355 extends DAO {
    public ThanhvienDAO355() {
        super();
    }

    public Thanhvien355 getUserById(int id) throws SQLException {
        String sql = "Select * from thanhvien355 where id = ?";
        CallableStatement cs = con.prepareCall(sql);
        cs.setInt(1, id);
        ResultSet rs = cs.executeQuery();

        if(rs.next()) {
            return new Thanhvien355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getDate("ngaySinh"),
                    rs.getString("diaChi"),
                    rs.getString("email"),
                    rs.getString("vaiTro"),
                    rs.getString("avatar"),
                    rs.getString("ghiChu")
            );
        }
        return null;
    }

    public Thanhvien355 getUserByUsername(String username) throws SQLException {
        String sql = "Select * from thanhvien355 where username = ?";
        CallableStatement cs = con.prepareCall(sql);
        cs.setString(1, username);
        ResultSet rs = cs.executeQuery();

        if(rs.next()) {
            return new Thanhvien355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getDate("ngaySinh"),
                    rs.getString("diaChi"),
                    rs.getString("email"),
                    rs.getString("vaiTro"),
                    rs.getString("avatar"),
                    rs.getString("ghiChu")
            );
        }
        return null;
    }

    public void createUser(Thanhvien355 u) throws SQLException {
        String sql = "Insert into thanhvien355 (ten, username, password, ngaySinh, diaChi, email, vaiTro, avatar, ghiChu) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        CallableStatement cs = con.prepareCall(sql);
        cs.setString(1, u.getTen());
        cs.setString(2, u.getUsername());
        cs.setString(3, u.getPassword());
        cs.setDate(4, u.getNgaySinh());
        cs.setString(5, u.getDiaChi());
        cs.setString(6, u.getEmail());
        cs.setString(7, u.getVaiTro());
        cs.setString(8, u.getAvatar());
        cs.setString(9, u.getGhiChu());
        Boolean bl = cs.execute();
    }
}
