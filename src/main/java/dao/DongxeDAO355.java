package dao;

import model.Dongxe355;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DongxeDAO355 extends DAO {
    public DongxeDAO355() {
        super();
    }

    public List<Dongxe355> getDSDongxe(int idHang) throws SQLException {
        String sql = "Select * from dongxe355 where hangxe355id = ?";
        CallableStatement cs = con.prepareCall(sql);
        cs.setInt(1, idHang);
        ResultSet rs = cs.executeQuery();
        List<Dongxe355> dsDongXe = new ArrayList();

        while (rs.next()) {
            Dongxe355 dongXe = new Dongxe355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getString("moTa"),
                    null
            );

            dsDongXe.add(dongXe);
        }
        return dsDongXe;
    }
}
