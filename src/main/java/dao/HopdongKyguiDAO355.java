package dao;

import model.Hangxe355;
import model.HopdongKygui355;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HopdongKyguiDAO355 extends DAO {
    public HopdongKyguiDAO355() {
        super();
    }

    public boolean taoHopdongKygui(HopdongKygui355 hd) throws SQLException {
        String checkExists = "Select * from hopdongkygui355 where ((? >= ngayBatDau and ? < ngayKetThuc)" +
                " or (? > ngayBatDau and ? <= ngayKetThuc) or (? < ngayBatDau and ? > ngayKetThuc)) and oto355id = ?";
        CallableStatement csCheck = con.prepareCall(checkExists);
        csCheck.setDate(1, hd.getNgayBatDau());
        csCheck.setDate(2, hd.getNgayBatDau());
        csCheck.setDate(3, hd.getNgayKetThuc());
        csCheck.setDate(4, hd.getNgayKetThuc());
        csCheck.setDate(5, hd.getNgayBatDau());
        csCheck.setDate(6, hd.getNgayKetThuc());
        csCheck.setInt(7, hd.getOto355id());
        ResultSet rs = csCheck.executeQuery();
        if (rs.next()) {
            return false;
        }

        String sql = "Insert into hopdongkygui355 (ngayTao, soTienThue, ngayBatDau, ngayKetThuc, oto355id, thanhvien355id, tinhTrangXe) values (?, ?, ?, ?, ?, ?, ?)";
        CallableStatement cs = con.prepareCall(sql);
        cs.setTimestamp(1, hd.getNgayTao());
        cs.setInt(2, hd.getSoTienThue());
        cs.setDate(3, hd.getNgayBatDau());
        cs.setDate(4, hd.getNgayKetThuc());
        cs.setInt(5, hd.getOto355id());
        cs.setInt(6, hd.getThanhvien355id());
        cs.setString(7, hd.getTinhTrangXe());
        Boolean bl = cs.execute();

        return true;
    }
}
