package dao;

import model.Doitac355;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DoitacDAO355 extends DAO {
    public DoitacDAO355() {
        super();
    }

    public List<Doitac355> timDoitac(Integer soGhe, Integer idHang, Integer idDongxe, String ten) throws SQLException {
        String where = "";
        if(soGhe != null) {
            where += " and oto355.soGheNgoi = " + soGhe;
        }
        if(idHang != null) {
            where += " and dongxe355.hangxe355id = " + idHang;
        }
        if(idDongxe != null) {
            where += " and oto355.dongxe355id = " + idDongxe;
        }
        if(ten != null && ten.length() > 0) {
            where += " and oto355.ten like '%" + ten + "%'";
        }
        String sql = "Select * from doitac355 where" +
                " exists(select * from oto355 left join dongxe355 on dongxe355id = dongxe355.id " +
                " where doitac355id = doitac355.id" + where + ")";
        CallableStatement cs = con.prepareCall(sql);
        ResultSet rs = cs.executeQuery();
        List<Doitac355> dsDoitac = new ArrayList();

        while (rs.next()) {
            Doitac355 doitac = new Doitac355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getDate("ngaySinh"),
                    rs.getString("diaChi"),
                    rs.getString("sdt"),
                    rs.getString("ghiChu")
            );

            dsDoitac.add(doitac);
        }
        return dsDoitac;
    }
}
