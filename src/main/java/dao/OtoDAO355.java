package dao;

import model.*;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OtoDAO355 extends DAO {
    public OtoDAO355() {
        super();
    }

    public List<Integer> getDSSoghe() throws SQLException {
        String sql = "Select distinct(soGheNgoi) from oto355";
        CallableStatement cs = con.prepareCall(sql);
        ResultSet rs = cs.executeQuery();
        List<Integer> dsGhe = new ArrayList();

        while (rs.next()) {
            dsGhe.add(rs.getInt("soGheNgoi"));
        }
        return dsGhe;
    }

    public List<Oto355> getDSXe(int idDoitac) throws SQLException {
        String sql = "Select oto355.id, oto355.ten, bienSo, doiXe, soGheNgoi, oto355.moTa, doitac355id, dongxe355id, hangxe355id, ngaySinh," +
                " diaChi, sdt, ghiChu, dongxe355.ten as dxTen, dongxe355.moTa as dxMoTa, hangxe355.ten as hxTen, hangxe355.moTa as hxMoTa, doitac355.ten as dtTen" +
                " from oto355" +
                " left join dongxe355 on dongxe355.id = oto355.dongxe355id" +
                " left join hangxe355 on hangxe355.id = dongxe355.hangxe355id" +
                " left join doitac355 on doitac355id = doitac355.id" +
                " where doitac355id = ?";
        CallableStatement cs = con.prepareCall(sql);
        cs.setInt(1, idDoitac);
        ResultSet rs = cs.executeQuery();
        List<Oto355> dsXe = new ArrayList();

        while (rs.next()) {
            Doitac355 dt = new Doitac355(
                    rs.getInt("doitac355id"),
                    rs.getString("dtTen"),
                    rs.getDate("ngaySinh"),
                    rs.getString("diaChi"),
                    rs.getString("sdt"),
                    rs.getString("ghiChu")
            );

            Hangxe355 hangXe = new Hangxe355(
                    rs.getInt("hangxe355id"),
                    rs.getString("hxTen"),
                    rs.getString("hxMoTa")
            );

            Dongxe355 dongXe = new Dongxe355(
                    rs.getInt("dongxe355id"),
                    rs.getString("dxTen"),
                    rs.getString("dxMoTa"),
                    hangXe
            );

            Oto355 xe = new Oto355(
                    rs.getInt("id"),
                    rs.getString("ten"),
                    rs.getString("bienSo"),
                    rs.getInt("doiXe"),
                    rs.getInt("soGheNgoi"),
                    rs.getString("moTa"),
                    rs.getInt("doitac355id"),
                    rs.getInt("dongxe355id"),
                    dt,
                    dongXe
            );

            dsXe.add(xe);
        }
        return dsXe;
    }
}
