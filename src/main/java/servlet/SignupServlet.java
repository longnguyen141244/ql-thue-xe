package servlet;

import com.google.gson.Gson;
import dao.ThanhvienDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Thanhvien355;
import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(value = "/signup")
public class SignupServlet extends HttpServlet {
    private Gson gson;
    private ThanhvienDAO355 userDAO;

    public void init() {
        gson = new Gson();
        userDAO = new ThanhvienDAO355();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        Thanhvien355 user = gson.fromJson(request.getReader(), Thanhvien355.class);

        try {
            Thanhvien355 currentUser = userDAO.getUserByUsername(user.getUsername());
            if (currentUser != null) {
                response.setStatus(400);
                out.println(gson.toJson(new ErrorResponse("Tài khoản đã tồn tại!")));
                return;
            }

            String hashPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(12));
            user.setPassword(hashPassword);
            userDAO.createUser(user);

        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
