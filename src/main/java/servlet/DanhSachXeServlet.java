package servlet;

import com.google.gson.Gson;
import dao.OtoDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Oto355;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = "/admin/danh-sach-xe")
public class DanhSachXeServlet extends HttpServlet {
    private Gson gson;
    private OtoDAO355 otoDAO355;

    public void init() {
        gson = new Gson();
        otoDAO355 = new OtoDAO355();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        int idDoitac = Integer.parseInt(request.getParameter("idDoitac"));

        try {
            List<Oto355> dsXe = otoDAO355.getDSXe(idDoitac);
            out.println(gson.toJson(dsXe));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
