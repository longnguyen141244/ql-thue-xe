package servlet;

import com.google.gson.Gson;
import dao.HangxeDAO355;
import dao.OtoDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Hangxe355;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = "/admin/danh-sach-hang-xe")
public class DanhSachHangXeServlet extends HttpServlet {
    private Gson gson;
    private HangxeDAO355 hangxeDAO355;

    public void init() {
        gson = new Gson();
        hangxeDAO355 = new HangxeDAO355();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();

        try {
            List<Hangxe355> dsHangXe = hangxeDAO355.getDSHangxe();
            out.println(gson.toJson(dsHangXe));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
