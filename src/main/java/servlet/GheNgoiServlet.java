package servlet;

import com.google.gson.Gson;
import dao.OtoDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = "/admin/so-ghe-ngoi")
public class GheNgoiServlet extends HttpServlet {
    private Gson gson;
    private OtoDAO355 otoDAO355;

    public void init() {
        gson = new Gson();
        otoDAO355 = new OtoDAO355();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();

        try {
            List<Integer> dsGhe = otoDAO355.getDSSoghe();
            out.println(gson.toJson(dsGhe));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
