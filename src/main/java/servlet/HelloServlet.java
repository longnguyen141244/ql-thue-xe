package servlet;

import java.io.*;

import com.google.gson.Gson;
import dao.ThanhvienDAO355;
import helper.AuthenticatedRequest;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(value = "/admin/json")
public class HelloServlet extends HttpServlet {
    private Gson gson;
    private ThanhvienDAO355 userDAO;

    public void init() {
        gson = new Gson();
        userDAO = new ThanhvienDAO355();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();

        AuthenticatedRequest au = (AuthenticatedRequest) request;
        out.println(gson.toJson(au.getUser()));
    }

    public void destroy() {
    }
}

