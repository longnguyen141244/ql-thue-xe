package servlet;

import com.google.gson.Gson;
import dao.DongxeDAO355;
import dao.HangxeDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Dongxe355;
import model.Hangxe355;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = "/admin/danh-sach-dong-xe")
public class DanhSachDongXeServlet extends HttpServlet {
    private Gson gson;
    private DongxeDAO355 dongxeDAO355;

    public void init() {
        gson = new Gson();
        dongxeDAO355 = new DongxeDAO355();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        int idHang = Integer.parseInt(request.getParameter("idHang"));

        try {
            List<Dongxe355> dsDongXe = dongxeDAO355.getDSDongxe(idHang);
            out.println(gson.toJson(dsDongXe));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
