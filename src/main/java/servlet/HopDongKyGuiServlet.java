package servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.HopdongKyguiDAO355;
import helper.AuthenticatedRequest;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.HopdongKygui355;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;

@WebServlet(value = "/admin/tao-hop-dong")
public class HopDongKyGuiServlet extends HttpServlet {
    static String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private Gson gson;
    private HopdongKyguiDAO355 hopdongKyguiDAO355;

    public void init() {
        gson = new GsonBuilder().setDateFormat(DATE_PATTERN).create();
        hopdongKyguiDAO355 = new HopdongKyguiDAO355();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        AuthenticatedRequest newRequest = (AuthenticatedRequest) request;
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        HopdongKygui355 hd = gson.fromJson(request.getReader(), HopdongKygui355.class);
        hd.setNgayTao(new Timestamp(System.currentTimeMillis()));
        hd.setThanhvien355id(newRequest.getUser().getId());

        try {
            boolean result = hopdongKyguiDAO355.taoHopdongKygui(hd);
            if (!result) {
                response.setStatus(400);
                out.println(gson.toJson(new ErrorResponse("Tạo hợp đồng thất bại, thời gian thuê xe trùng nhau!")));
                return;
            }
            out.println(gson.toJson(result));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
