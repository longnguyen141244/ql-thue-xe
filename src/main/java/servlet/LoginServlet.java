package servlet;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import dao.ThanhvienDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Thanhvien355;
import org.mindrot.jbcrypt.BCrypt;
import response_class.Token;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;

@WebServlet(value = "/login")
public class LoginServlet extends HttpServlet {
    private Gson gson;
    private ThanhvienDAO355 userDAO;

    public void init() {
        gson = new Gson();
        userDAO = new ThanhvienDAO355();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        Thanhvien355 user = gson.fromJson(request.getReader(), Thanhvien355.class);

        try {
            Thanhvien355 currentUser = userDAO.getUserByUsername(user.getUsername());
            if (currentUser == null) {
                response.setStatus(401);
                out.println(gson.toJson(new ErrorResponse("Tài khoản hoặc mật khẩu không chính xác!")));
            }
            boolean matched = BCrypt.checkpw(user.getPassword(), currentUser.getPassword());
            if (!matched) {
                response.setStatus(401);
                out.println(gson.toJson(new ErrorResponse("Tài khoản hoặc mật khẩu không chính xác!")));
            } else {
                currentUser.setPassword("");
                Algorithm algorithm = Algorithm.HMAC256("jwt_key_1412");
                String token = JWT.create()
                        .withIssuer(gson.toJson(currentUser))
                        .withIssuedAt(new Timestamp(System.currentTimeMillis()))
                        .withExpiresAt(new Timestamp(System.currentTimeMillis() + 12 * 60 * 60 * 1000))
                        .sign(algorithm);
                out.println(gson.toJson(new Token(token)));
            }
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
