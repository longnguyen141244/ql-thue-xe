package servlet;

import com.google.gson.Gson;
import dao.DoitacDAO355;
import dao.DongxeDAO355;
import helper.ErrorResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Doitac355;
import model.Dongxe355;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value = "/admin/tim-doi-tac")
public class TimDoiTacServlet extends HttpServlet {
    private Gson gson;
    private DoitacDAO355 doitacDAO355;

    public void init() {
        gson = new Gson();
        doitacDAO355 = new DoitacDAO355();
    }

    public static Integer parseInteger(String a) {
        try {
            return Integer.parseInt(a);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json");
        // Response
        PrintWriter out = response.getWriter();
        Integer soGhe = parseInteger(request.getParameter("soGhe"));
        Integer idHang = parseInteger(request.getParameter("idHang"));
        Integer idDongxe = parseInteger(request.getParameter("idDongxe"));
        String ten = request.getParameter("ten");

        try {
            List<Doitac355> dsDoitac = doitacDAO355.timDoitac(soGhe, idHang, idDongxe, ten);
            out.println(gson.toJson(dsDoitac));
        } catch (SQLException e) {
            response.setStatus(400);
            out.println(gson.toJson(new ErrorResponse("SQLException!")));
            System.out.println(e);
        }
    }

    public void destroy() {
    }
}
