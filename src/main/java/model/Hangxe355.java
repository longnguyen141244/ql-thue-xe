package model;

import java.io.Serializable;
import java.sql.Date;

public class Hangxe355 implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String ten;
    private String moTa;

    public Hangxe355() {
    }

    public Hangxe355(int id, String ten, String moTa) {
        this.id = id;
        this.ten = ten;
        this.moTa = moTa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }
}
