package model;

import java.io.Serializable;
import java.sql.Date;

public class Thanhvien355 implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String ten;
    private String username;
    private String password;
    private Date ngaySinh;
    private String diaChi;
    private String email;
    private String vaiTro;
    private String avatar;
    private String ghiChu;

    public Thanhvien355() {
    }

    public Thanhvien355(int id, String ten, String username, String password) {
        this.id = id;
        this.ten = ten;
        this.username = username;
        this.password = password;
    }

    public Thanhvien355(int id, String ten, String username, String password, Date ngaySinh, String diaChi, String email, String vaiTro, String avatar, String ghiChu) {
        this.id = id;
        this.ten = ten;
        this.username = username;
        this.password = password;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
        this.email = email;
        this.vaiTro = vaiTro;
        this.avatar = avatar;
        this.ghiChu = ghiChu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVaiTro() {
        return vaiTro;
    }

    public void setVaiTro(String vaiTro) {
        this.vaiTro = vaiTro;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
}
