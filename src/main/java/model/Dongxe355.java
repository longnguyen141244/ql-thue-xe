package model;

import java.io.Serializable;

public class Dongxe355 implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String ten;
    private String moTa;
    private Hangxe355 hangXe;

    public Dongxe355() {
    }

    public Dongxe355(int id, String ten, String moTa, Hangxe355 hangXe) {
        this.id = id;
        this.ten = ten;
        this.moTa = moTa;
        this.hangXe = hangXe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Hangxe355 getHangXe() {
        return hangXe;
    }

    public void setHangXe(Hangxe355 hangXe) {
        this.hangXe = hangXe;
    }
}
