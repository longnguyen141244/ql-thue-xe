package model;

import java.io.Serializable;
import java.sql.Date;

public class Oto355 implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String ten;
    private String bienSo;
    private int doiXe;
    private int soGheNgoi;
    private String moTa;
    private int doitac355id;
    private int dongxe355id;
    private Doitac355 doiTac;
    private Dongxe355 dongXe;

    public Oto355() {
    }

    public Oto355(int id, String ten, String bienSo, int doiXe, int soGheNgoi, String moTa, int doitac355id, int dongxe355id, Doitac355 doiTac, Dongxe355 dongXe) {
        this.id = id;
        this.ten = ten;
        this.bienSo = bienSo;
        this.doiXe = doiXe;
        this.soGheNgoi = soGheNgoi;
        this.moTa = moTa;
        this.doitac355id = doitac355id;
        this.dongxe355id = dongxe355id;
        this.doiTac = doiTac;
        this.dongXe = dongXe;
    }
}
