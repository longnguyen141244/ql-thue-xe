package model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

public class HopdongKygui355 implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private Timestamp ngayTao;
    private String tinhTrangXe;
    private int soTienThue;
    private Date ngayBatDau;
    private Date ngayKetThuc;
    private int oto355id;
    private int thanhvien355id;
    private Thanhvien355 tv;
    private Oto355 xe;

    public HopdongKygui355() {
    }

    public HopdongKygui355(int id, Timestamp ngayTao, String tinhTrangXe, int soTienThue, Date ngayBatDau, Date ngayKetThuc, int oto355id, int thanhvien355id) {
        this.id = id;
        this.ngayTao = ngayTao;
        this.tinhTrangXe = tinhTrangXe;
        this.soTienThue = soTienThue;
        this.ngayBatDau = ngayBatDau;
        this.ngayKetThuc = ngayKetThuc;
        this.oto355id = oto355id;
        this.thanhvien355id = thanhvien355id;
    }

    public HopdongKygui355(int id, Timestamp ngayTao, String tinhTrangXe, int soTienThue, Date ngayBatDau, Date ngayKetThuc, Thanhvien355 tv, Oto355 xe) {
        this.id = id;
        this.ngayTao = ngayTao;
        this.tinhTrangXe = tinhTrangXe;
        this.soTienThue = soTienThue;
        this.ngayBatDau = ngayBatDau;
        this.ngayKetThuc = ngayKetThuc;
        this.tv = tv;
        this.xe = xe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getTinhTrangXe() {
        return tinhTrangXe;
    }

    public void setTinhTrangXe(String tinhTrangXe) {
        this.tinhTrangXe = tinhTrangXe;
    }

    public int getSoTienThue() {
        return soTienThue;
    }

    public void setSoTienThue(int soTienThue) {
        this.soTienThue = soTienThue;
    }

    public Date getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(Date ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public Date getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(Date ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public int getOto355id() {
        return oto355id;
    }

    public void setOto355id(int oto355id) {
        this.oto355id = oto355id;
    }

    public int getThanhvien355id() {
        return thanhvien355id;
    }

    public void setThanhvien355id(int thanhvien355id) {
        this.thanhvien355id = thanhvien355id;
    }

    public Thanhvien355 getTv() {
        return tv;
    }

    public void setTv(Thanhvien355 tv) {
        this.tv = tv;
    }

    public Oto355 getXe() {
        return xe;
    }

    public void setXe(Oto355 xe) {
        this.xe = xe;
    }
}
