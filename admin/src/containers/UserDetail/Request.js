import React from 'react'
import { Button, Typography, message } from 'antd'
import api from '../../api'
import useNotification from '../../hooks/useNotification'

const ItemText = ({ changeFunc, label, value }) => {
  return (
    <span style={{ display: 'flex', justifyContent: 'space-between', paddingTop: 10 }}>
      <Typography.Text strong style={{ flex: 1, paddingRight: 10 }}>
        {label}{' '}
      </Typography.Text>
      <Typography.Text style={{ flex: 2 }}>{value}</Typography.Text>
    </span>
  )
}
export default function Request({ data, dispatchReload }) {
  const [ROLE] = React.useState(localStorage.getItem('role'))
  const { pushNotifications } = useNotification()

  const request = data ? data.filter(item => item.status === 'pending')[0] : null
  async function sumbitRequest() {
    await api.put(`/requests/${request._id}`, { status: 'accepted', amount: request.amount })
    pushNotifications({
      to: request.userId,
      message: 'Yêu cầu rút tiền được duyệt',
      description: 'Hệ thống đã xác nhận yêu cầu rút tiền của bạn. Rút tiền thành công.',
    })
    message.success('Cập nhật thành công')
    dispatchReload()
  }
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        background: '#fff',
        padding: 20,
        borderRadius: 10,
        minWidth: 420,
        flex: 1,
        margin: '0px 10px',
        maxHeight: 400,
      }}
    >
      {request ? (
        <>
          <Typography.Title level={5} style={{ textAlign: 'center' }}>
            Yêu cầu rút tiền
          </Typography.Title>
          <hr />
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              minHeight: '50%',
            }}
          >
            <ItemText
              label="Số tiền yêu cầu"
              value={request?.amount ? request?.amount.toLocaleString() : 0}
            />
            {ROLE == 'ROOT' && (
              <div style={{ justifyContent: 'center', display: 'flex' }}>
                <Button type="primary" onClick={sumbitRequest}>
                  Duyệt
                </Button>
              </div>
            )}
          </div>
        </>
      ) : (
        <Typography.Text strong style={{ textAlign: 'center' }}>
          Hiện không có yêu cầu
        </Typography.Text>
      )}
    </div>
  )
}
