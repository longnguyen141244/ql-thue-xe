import React, { useEffect, useState } from 'react'
import api from '../api'
import { Table, Typography, message, Select } from 'antd'
import { useHistory, useLocation } from 'react-router-dom'
import GDTaohopdong355 from './GDTaohopdong355'

export default function Request(props) {
  const [metadata, setMetadata] = useState([])
  const [doiTac, setDoiTac] = useState({})
  const [xe, setXe] = useState({})
  const [openDetail, setOpenDetail] = useState(false)
  const history = useHistory()
  const query = new URLSearchParams(useLocation().search)

  useEffect(() => {
    loadData()
    // history.replace('/?page=1&search=&searchId=&meta=1,2,3')
  }, [])

  const loadData = async () => {
    try {
      const idDoitac = query.get('idDoitac')
      if (!idDoitac) {
        message.error('ID đối tác không hợp lệ!')
        return
      }
      const { data } = await api.get('/admin/danh-sach-xe', {
        params: {
          idDoitac
        }
      })

      setMetadata(data)
      if (data && data.length) {
        setDoiTac(data[0]?.doiTac)
      }
    } catch (error) {
      message.error(error.message)
    }
  }

  const columns = [
    {
      title: 'Mã',
      key: 'id',
      render: (text, record) => <Typography.Text>{record?.id}</Typography.Text>,
    },
    {
      title: 'Tên xe',
      key: 'ten',
      render: (text, record) => <Typography.Text>{record?.ten}</Typography.Text>,
    },
    {
      title: 'Biển số',
      key: 'bienSo',
      render: (text, record) => <Typography.Text>{record?.bienSo}</Typography.Text>,
    },
    {
      title: 'Hãng xe',
      key: 'tenHang',
      render: (text, record) => (
        <Typography.Text>{record?.dongXe?.hangXe?.ten}</Typography.Text>
      ),
    },
    {
      title: 'Dòng xe',
      key: 'tenDongXe',
      render: (text, record) => (
        <Typography.Text>{record?.dongXe?.ten}</Typography.Text>
      ),
    },
    {
      title: 'Đời xe',
      key: 'doiXe',
      render: (text, record) => (
        <Typography.Text>{record?.doiXe}</Typography.Text>
      ),
    },
    {
      title: 'Số ghế',
      key: 'soGhe',
      render: (text, record) => (
        <Typography.Text>{record?.soGheNgoi}</Typography.Text>
      ),
    },
    {
      title: 'Mô tả',
      key: 'moTa',
      render: (text, record) => (
        <Typography.Text>
          {record?.moTa}
        </Typography.Text>
      ),
    },
    {
      title: 'Hành động',
      key: 'response',
      render: (text, record) => (
        <Typography.Text className='btn-dsx' onClick={() => {
          setXe(record)
          setOpenDetail(true)
        }}>
          Tạo hợp đồng
        </Typography.Text>
      ),
    },
  ]
  return (
    <div>
      <div className='tt-doi-tac'>
        <div className='info-item'>
          <div>Tên đối tác: </div>
          <div>{doiTac?.ten}</div>
        </div>
        <div className='info-item'>
          <div>Số điện thoại: </div>
          <div>{doiTac?.sdt}</div>
        </div>
      </div>
      <Table columns={columns} dataSource={metadata} pagination={false} />
      <GDTaohopdong355 open={openDetail} data={xe} onClose={() => setOpenDetail(false)}/>
    </div>
  )
}
