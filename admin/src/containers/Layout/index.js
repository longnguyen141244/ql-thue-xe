import React from 'react'
import { Layout, Menu, Switch, Typography, Space, Avatar, Popconfirm } from 'antd'
import {
  ProfileOutlined,
  PoweroffOutlined,
  LineChartOutlined,
  HomeOutlined,
  UserOutlined,
  FormOutlined,
} from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
import './index.scss'
import logo from './logo.png'
import * as actions from '../../redux/actions/auth'
import { useDispatch } from 'react-redux'
import { Notifications } from '../../components'

const { Header, Sider, Content } = Layout
export default function Home({ children }) {
  const [mode, setMode] = React.useState(false)
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <SiderMenu mode={mode} />
      <Layout>
        <HeaderCustom setMode={setMode} mode={mode} />
        <Content style={{ padding: 20 }}>{children}</Content>
      </Layout>
    </Layout>
  )
}

const HeaderCustom = ({ setMode, mode }) => {
  const dispatch = useDispatch()
  function logout() {
    dispatch(actions.Logout())
  }
  return (
    <Header className="header-bar" style={{ background: '#001529' }}>
      <div></div>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingRight: 20,
          minWidth: 200,
        }}
      >
        <Popconfirm title="Xác nhận đăng xuất" onConfirm={logout}>
          <div style={{cursor: 'pointer'}}>
            <Avatar src="https://khoinguonsangtao.vn/wp-content/uploads/2022/07/avatar-gau-cute.jpg" alt='' />
            <span style={{fontWeight: '600', color: '#fff', marginLeft: '10px'}}>Nguyễn Thành Long</span>
          </div>
        </Popconfirm>
      </div>
    </Header>
  )
}

const SiderMenu = ({ mode }) => {
  const history = useHistory()
  const [key] = React.useState(1)
  return (
    <Sider width={250}>
      <Menu
        mode="inline"
        theme='light'
        style={{
          height: '100%',
          borderRight: 0,
          minHeight: '100vh',
        }}
        activeKey={key}
      >
        <div
          style={{
            background: '#001529',
            padding: '8px 20px'
          }}
        >
          <img src='logo.png' alt='' />
        </div>
        <Menu.Item
          key="1"
          icon={<HomeOutlined />}
          onClick={() => {
            history.push('/')
          }}
        >
          Trang chủ
        </Menu.Item>
        <Menu.SubMenu
          key="2"
          icon={<FormOutlined />}
          title="Đối tác"
        >
          <Menu.Item
            key="2.1"
            onClick={() => {
              history.push('/tim-doi-tac')
            }}
          >
            Tạo hợp đồng ký gửi
          </Menu.Item>
          <Menu.Item
            key="2.2"
          >
            Danh sách hợp đồng
          </Menu.Item>
          <Menu.Item
            key="2.3"
          >
            Danh sách đối tác
          </Menu.Item>
        </Menu.SubMenu>
        <Menu.SubMenu
          key="5"
          icon={<ProfileOutlined />}
          title="Quản lý khách hàng"
        >
          
        </Menu.SubMenu>
        <Menu.SubMenu
          key="3"
          icon={<UserOutlined />}
          title="Quản lý tài khoản"
        >
          
        </Menu.SubMenu>
        <Menu.SubMenu
          key="4"
          icon={<LineChartOutlined />}
          title="Báo cáo thống kê"
        >
          
        </Menu.SubMenu>
      </Menu>
    </Sider>
  )
}
