import React, { useEffect, useState } from 'react'
import api from '../api'
import { Typography, message, Form, Select, Input, Button, Modal, DatePicker } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import locale from 'antd/es/date-picker/locale/vi_VN'
import { useHistory } from 'react-router-dom'
import moment from 'moment'
const { Option } = Select

export default function Request({ open, onClose, data }) {
  const [form] = Form.useForm();
  const history = useHistory()

  useEffect(() => {

  }, [])

  const onFinish = async (values) => {
    try {
      const { data: apiRes } = await api.post('/admin/tao-hop-dong', {
        tinhTrangXe: values.tinhTrangXe,
        soTienThue: parseInt(values.soTienThue),
        ngayBatDau: values.ngayBatDau?.format("YYYY-MM-DD 00:00:00"),
        ngayKetThuc: values.ngayKetThuc?.format("YYYY-MM-DD 00:00:00"),
        oto355id: data?.id
      })

      onClose()
      history.push('/tim-doi-tac')
      message.success("Tạo hợp đồng ký gửi thành công!")
    } catch (error) {
      message.error(error.message)
    }
  }

  const _validateInput = async (_, value) => {
    if (value && value.length > 255) {
      return Promise.reject('Nhập tối đa 255 ký tự!')
    }
    return Promise.resolve()
  }

  const _validateInputInt = async (_, value) => {
    if (value && !value.match(/^\d+$/)) {
      return Promise.reject('Vui lòng nhập số nguyên dương!')
    }
    return Promise.resolve()
  }

  const _validateInputStartDate = async (_, value) => {
    const endDate = form.getFieldValue("ngayKetThuc")
    if (value && endDate && value.format("YYYY-MM-DD 00:00:00") >= endDate.format("YYYY-MM-DD 00:00:00")) {
      return Promise.reject('Vui lòng chọn ngày bắt đầu nhỏ hơn ngày kết thúc!')
    }
    return Promise.resolve()
  }

  const onDateChange = () => {
    form.validateFields(["ngayBatDau"])
  }

  return (
    <Modal
      visible={open}
      onOk={onClose}
      onCancel={() => {
        form.resetFields()
        onClose()
      }}
      maskClosable={false}
      cancelButtonProps={true}
      title="Tạo hợp đồng ký gửi"
      footer={false}
      centered
      width={600}
      className="hd-modal"
    >
      <Form form={form} name="tao-hop-dong" 
        onFinish={onFinish}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        className="hd-form"
        initialValues={false}
      >
        <Form.Item
          className='form-item'
          label="Tên xe:"
        >
          <Typography.Text>{data?.ten}</Typography.Text>
        </Form.Item>
        <Form.Item
          className='form-item'
          label="Biển số:"
        >
          <Typography.Text>{data?.bienSo}</Typography.Text>
        </Form.Item>
        <Form.Item
          name='tinhTrangXe'
          className='form-item'
          label="Tình trạng xe:"
          rules={[
            { required: true, message: 'Vui lòng nhập tình trạng xe!' },
            { validator: _validateInput }
          ]}
        >
          <Input placeholder='Nhập tình trạng xe'/>
        </Form.Item>
        <Form.Item
          name='soTienThue'
          className='form-item'
          label="Giá ký gửi:"
          rules={[
            { required: true, message: 'Vui lòng nhập giá ký gửi!' },
            { validator: _validateInputInt }
          ]}
        >
          <Input type='number' placeholder='Nhập giá ký gửi'/>
        </Form.Item>
        <Form.Item
          name='ngayBatDau'
          className='form-item'
          label="Ngày bắt đầu:"
          rules={[
            { required: true, message: 'Vui lòng chọn ngày bắt đầu!' },
            { validator: _validateInputStartDate }
          ]}
        >
          <DatePicker style={{width: '100%'}} placeholder='Chọn ngày bắt đầu' locale={locale}
            disabledDate={(current) => {
              return moment().add(-1, 'days')  >= current
            }}
          />
        </Form.Item>
        <Form.Item
          name='ngayKetThuc'
          className='form-item'
          label="Ngày kết thúc:"
          rules={[{ required: true, message: 'Vui lòng chọn ngày kết thúc!' }]}
        >
          <DatePicker style={{width: '100%'}} placeholder='Chọn ngày kết thúc' locale={locale} onChange={onDateChange}/>
        </Form.Item>

        <div className='btn-create-block'>
          <Button type="primary" htmlType="submit" className='btn-create'>
            Tạo hợp đồng
          </Button>
        </div>
      </Form>
    </Modal>
  )
}
