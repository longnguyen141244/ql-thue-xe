import React, { useEffect, useState } from 'react'
import api from '../api'
import { Table, Typography, message, Form, Select, Input, Button } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom'
const { Option } = Select

export default function Request() {
  const [metadata, setMetadata] = useState([])
  const [dsGhe, setDsGhe] = useState([])
  const [dsHang, setDsHang] = useState([])
  const [dsDongXe, setDsDongXe] = useState([])
  const [form] = Form.useForm();
  const history = useHistory()

  useEffect(() => {
    loadData()
    // history.replace('/?page=1&search=&searchId=&meta=1,2,3')
  }, [])

  const loadData = async () => {
    try {
      const { data: listSoGhe } = await api.get('/admin/so-ghe-ngoi')
      setDsGhe(listSoGhe)

      const { data: listHang } = await api.get('/admin/danh-sach-hang-xe')
      setDsHang(listHang)

      // const { data: listDongXe } = await api.get('/admin/so-ghe-ngoi')
      // setDsGhe(listSoGhe)
    } catch (error) {
      message.error(error.message)
    }
  }

  const hangOnChange = async (value) => {
    try {
      form.setFieldsValue({ idDongxe: null })
      if (!value) {
        setDsDongXe([])
        return
      }

      const { data: listDongXe } = await api.get('/admin/danh-sach-dong-xe', { params: { idHang: value } })
      setDsDongXe(listDongXe)
    } catch (error) {
      message.error(error.message)
    }
  }

  const onFinish = async (values) => {
    try {
      if (!values.soGhe && !values.idHang && !values.idDongxe && !values.ten) {
        message.error("Bạn cần nhập ít nhất một điều kiện để tìm kiếm!")
        return
      }

      const { data } = await api.get('/admin/tim-doi-tac', {
        params: values
      })

      setMetadata(data)
      if (data.length === 0) {
        message.error("Không tìm thấy đối tác!")
      }
    } catch (error) {
      message.error(error.message)
    }
  }

  const columns = [
    {
      title: 'Mã',
      key: 'id',
      render: (text, record) => <Typography.Text>{record.id}</Typography.Text>,
    },
    {
      title: 'Tên',
      key: 'ten',
      render: (text, record) => <Typography.Text>{record.ten}</Typography.Text>,
    },
    {
      title: 'Địa chỉ',
      key: 'diaChi',
      render: (text, record) => <Typography.Text>{record.diaChi}</Typography.Text>,
    },
    {
      title: 'Số điện thoại',
      key: 'sdt',
      render: (text, record) => (
        <Typography.Text>{record.sdt}</Typography.Text>
      ),
    },
    {
      title: 'Ghi chú',
      key: 'ghiChu',
      render: (text, record) => (
        <Typography.Text>
          {record.ghiChu}
        </Typography.Text>
      ),
    },
    {
      title: 'Hành động',
      key: 'response',
      render: (text, record) => (
        <Typography.Text className='btn-dsx' onClick={() => history.push("/ds-xe?idDoitac=" + record.id)}>
          Xem chi tiết
        </Typography.Text>
      ),
    },
  ]

  const _validateInput = async (_, value) => {
    if (value && value.length > 255) {
      return Promise.reject('Nhập tối đa 255 ký tự!')
    }
    return Promise.resolve()
  }

  return (
    <div>
      <div className='search-container'>
        <Form form={form} name="tim-doi-tac" onFinish={onFinish} className="tim-dt-form">
          <Form.Item name="soGhe" className='form-item'>
            <Select
              placeholder="Chọn số ghế ngồi"
              allowClear
            >
              {
                dsGhe.map(item => {
                  return <Option value={item}>{item}</Option>
                })
              }
              
            </Select>
          </Form.Item>
          <Form.Item name="idHang" className='form-item'>
            <Select
              placeholder="Chọn hãng xe"
              allowClear
              onChange={hangOnChange}
            >
              {
                dsHang.map(item => {
                  return <Option value={item.id}>{item.ten}</Option>
                })
              }
            </Select>
          </Form.Item>
          <Form.Item name="idDongxe" className='form-item'>
            <Select
              placeholder="Chọn dòng xe"
              allowClear
            >
              {
                dsDongXe.map(item => {
                  return <Option value={item.id}>{item.ten}</Option>
                })
              }
            </Select>
          </Form.Item>
          <Form.Item name="ten" className='form-item'
            rules={[
              { validator: _validateInput }
            ]}
          >
            <Input placeholder='Nhập tên xe'/>
          </Form.Item>

          <Button icon={<SearchOutlined />} type="primary" htmlType="submit" className='btn-search'>
            Tìm kiếm
          </Button>
        </Form>
      </div>
      <Table columns={columns} dataSource={metadata} pagination={false} />
    </div>
  )
}
