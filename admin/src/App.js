import React from 'react'
import _store from './redux/store'
import { BrowserRouter as Router, Switch, Route, useHistory, useLocation } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { Provider, useSelector, useDispatch } from 'react-redux'
import { Login, Layout, GDChonxe355, Statistics, GDTimdoitac355 } from './containers'
import * as actions from './redux/actions/auth'
import './App.scss'
const _history = createBrowserHistory()

const AuthorizedRoutes = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const { status } = useSelector(state => state._auth)

  React.useEffect(() => {
    const tokenFromStorage = localStorage.getItem('token')

    if (tokenFromStorage) dispatch(actions.initialLogin())
    if (!tokenFromStorage) history.replace('/auth')
  }, [status])
  return (
    <>
      <>
        {status ? (
          <Layout>
            <Switch>
              <Route path="/" exact>
                <Statistics />
              </Route>
              <Route path="/tim-doi-tac">
                <GDTimdoitac355 />
              </Route>
              <Route path="/ds-xe">
                <GDChonxe355 />
              </Route>
              <Route path="/statistics">
                <Statistics />
              </Route>
            </Switch>
          </Layout>
        ) : (
          <Switch>
            <Route exact path="/auth">
              <Login />
            </Route>
          </Switch>
        )}
      </>
    </>
  )
}

function App() {
  return (
    <Provider store={_store}>
      <Router history={_history}>
        <AuthorizedRoutes />
      </Router>
    </Provider>
  )
}

export default App
