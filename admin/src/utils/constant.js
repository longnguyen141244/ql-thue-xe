module.exports = {
    CONFIG_KEY: {
        CSKH: 'CSKH',
        THOI_GIAN_VAY: 'THOI_GIAN_VAY',
        STK: 'STK',
        HAN_MUC: 'HAN_MUC',
        LAI_SUAT: 'LAI_SUAT'
    },
    
    BASE_URL: 'http://localhost:8080/api/',
}