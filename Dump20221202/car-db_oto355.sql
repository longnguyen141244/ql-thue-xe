CREATE DATABASE  IF NOT EXISTS `car-db` /*!40100 DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `car-db`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: car-db
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oto355`
--

DROP TABLE IF EXISTS `oto355`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oto355` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `bienSo` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `doiXe` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  `soGheNgoi` int NOT NULL,
  `moTa` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `doitac355id` int DEFAULT NULL,
  `dongxe355id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oto355`
--

LOCK TABLES `oto355` WRITE;
/*!40000 ALTER TABLE `oto355` DISABLE KEYS */;
INSERT INTO `oto355` VALUES (1,'Huyndai Accent 1.4AT','30H-54799','2021',5,'Phiên bản đặc biệt',1,1),(2,'Kia Cerato 2.0','30H-88888','2021',5,'Phiên bản giới hạn',1,2),(3,'Huyndai Santafe 2.0D','30K-00001','2022',7,'Máy dầu',2,3),(4,'Huyndai Santafe 2.0D','30G-99999','2019',7,'Máy dầu',3,3),(5,'Kia Sonet 1.4 Duluxe','30H-54789','2022',5,'Máy xăng',4,4);
/*!40000 ALTER TABLE `oto355` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-02 17:43:52
